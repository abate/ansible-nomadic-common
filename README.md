Nomadic Common
=========

A basic role to provision nomadic labs servers and tezos nodes

Role Variables
--------------

```
users:
  - { username: root , home: /root }

timezone: Europe/Paris

docker_storage_driver: overlay2

extra_packages:
  - less
  - htop
  - git
  - rsync
  - etckeeper
  - avahi-daemon

avahi_aliases: []
```

Dependencies
------------

Avahi Aliases script :

wget https://raw.githubusercontent.com/george-hawkins/avahi-aliases-notes/master/avahi-alias-python2 -O roles/nomadic-common/files/avahi-alias-python2

wget https://raw.githubusercontent.com/george-hawkins/avahi-aliases-notes/master/avahi-alias.service -O roles/nomadic-common/files/avahi-alias.service

Example Playbook
----------------

```
- name: initialize nomadic babylonnet node
  hosts: nomadic
  tags:
    - install
  become: yes
  tasks:
    - name: Configure ufw rules
      ufw: rule={{ item.rule }} port={{ item.port }} proto={{ item.proto }}
      with_items:
        - { rule: 'allow', port: '22', proto: 'tcp' }
        - { rule: 'allow', port: '80', proto: 'tcp' }
        - { rule: 'allow', port: '443', proto: 'tcp' }
        - { rule: 'allow', port: '9732', proto: 'tcp' }
      notify:
        - restart ufw
```

License
-------

AGPL3

Author Information
------------------

Copyright Nomadic Labs
Author Pietro Abate < pietro.abate@nomadic-labs.com>
